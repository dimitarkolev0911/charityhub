import { Component, OnInit } from '@angular/core';
import { User } from '../models/User';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../services/user-service';
import { Charity } from '../models/Charity';
import { CharityService } from '../services/charity-service';

@Component({
  selector: 'app-my-profile-page',
  templateUrl: './my-profile-page.component.html',
  styleUrls: ['./my-profile-page.component.scss']
})
export class MyProfilePageComponent implements OnInit {

  public user:User = new User('','','','',0,'','');
  public currentUsername:string;
  public myCharities:Charity[];
  public donatedCharities:Charity[];
  public participatedCharities:Charity[];
  constructor(private router:Router, private userService: UserService, 
              private activatedRouter:ActivatedRoute, private charityService:CharityService) {
      
   }

  ngOnInit(): void {
    this.currentUsername = this.activatedRouter.snapshot.params.username;
    console.log(this.currentUsername)
    this.userService.getUser(this.currentUsername).subscribe(
      (data:User)=>this.user=data
    )
    if(this.user === null){
      this.router.navigate(['/error']);
    }
    else{
      this.charityService.getAllCharitiesCreatedByUser(this.currentUsername).subscribe(
        (data:Charity[])=>this.myCharities = data
      )
      this.charityService.getAllCharityInWhichUserHasDonated(this.currentUsername).subscribe(
        (data:Charity[])=>this.donatedCharities = data
      )
      this.charityService.getAllCharityInWhichUserHasParticipatedIn(this.currentUsername).subscribe(
        (data:Charity[])=>this.participatedCharities = data
      )
    }
    

  }

}
